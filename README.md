# Install cuda 10.0 in ubuntu 18.04

#### Introudction

To record the installation of cuda 10.0 in ubuntu 18.04

#### Installation of nVidia driver in ubuntu 18.04

* Reference: https://www.maketecheasier.com/install-nvidia-drivers-ubuntu/
* Reference: https://www.tecmint.com/install-nvidia-drivers-on-ubuntu/
```
sudo apt update
sudo apt upgrade
ubuntu-drivers list
sudo apt install nvidia-driver-VERSION_NUMBER_HERE
```
* The method to purge nVidia driver
```
sudo apt --purge autoremove nvidia*
```

#### Installation of cuda 10 in ubuntu 18.04
* To check nvcc is active or not.
```
nvcc --version
```
* cuda 10.0 download reference: https://developer.nvidia.com/cuda-10.0-download-archive?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=1804&target_type=deblocal
```
sudo dpkg -i cuda-repo-ubuntu1804-10-0-local-10.0.130-410.48_1.0-1_amd64.deb
sudo apt-key add /var/cuda-repo-<version>/7fa2af80.pub
sudo apt-get update
sudo apt-get install cuda
```
* Sometimes you will met the issue of no cuda found by nvcc. You should assign the path to solve this. You can edit ~/.bashrc to append the below part.
```
export PATH=/usr/local/cuda-10.0/bin${PATH:+:$PATH}}      
export LD_LIBRARY_PATH=/usr/local/cuda-10.0/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}} 
export CUDA_HOME=/usr/local/cuda-10.0
```
* The method to purge cuda 10
```
sudo apt-get --purge remove cuda
sudo apt-get autoremove cuda
sudo apt-get remove cuda*
cd /usr/local/
sudo rm -r cuda-10.0
sudo apt-get autoclean
```

#### Installation of cuda in a simple way
* Reference: https://askubuntu.com/questions/530043/removing-nvidia-cuda-toolkit-and-installing-new-one
```
sudo apt-get remove nvidia-cuda-toolkit
sudo apt-get install nvidia-cuda-toolkit
```

#### Installation of nVidia beta drivers
* Reference: https://linuxconfig.org/how-to-install-the-nvidia-drivers-on-ubuntu-18-04-bionic-beaver-linux
```
ubuntu-drivers devices
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt update
sudo ubuntu-drivers autoinstall

```